# Comment lines
# # General makefile for c - choose PROG =   name of given program
# # Here we define compiler option, libraries and the  target
CC= g++ -Wall
PROG= main
#CXXFLAGS= -I /usr/include/openmpi-x86_64 -pthread
# # this is the math library in C, not necessary for C++
#LIB = -pthread -Wl,-rpath -Wl,/usr/lib64/openmpi/lib -Wl,--enable-new-dtags -L/usr/lib64/openmpi/lib -lmpi_cxx -lmpi
# # Here we make the executable file
${PROG} :	${PROG}.o
#		${CC} ${PROG}.o ${LIB} -o ${PROG}
		${CC} ${PROG}.o ${LIB} -o ${PROG}
#                    # whereas here we create the object file
${PROG}.o :	${PROG}.cpp
		${CC}  -c ${PROG}.cpp
clean:
	rm -f *.o