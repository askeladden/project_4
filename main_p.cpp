#include <cmath>
#include <time.h>
#include <fstream>
#include <iostream>  //cout
#include <stdlib.h> //rand
#include <map>
#include <vector>
#include <mpi.h>
using namespace std;


void energy_of_state_brute_force (int, int **, double &, double &, double &, double &);
void delta_energy_by_spin_flipping (int, int, int, int **, double &, double &);
void update_expect(double &, double &, double &, double &, double &, double &, double &, double &);
//void write_to_file ( char *, double, double, int );
void write_to_file ( char *, double, double, double, double, double );
void state_init (int, int **, int);

int main ( int argc, char *argv[] )
{

int numprocs, my_rank;

MPI_Init (&argc, &argv);
MPI_Comm_size (MPI_COMM_WORLD, &numprocs);
MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);

//how to use a programm
//input must be "Lattice size" "Number of Monte Carlo cycles"  "output file"
    if ( argc != 5 ) {
       cout<<"Usage: "<<argv[0]<<" [Lattice size] [Number of Monte Carlo cycles] [initial state = 1(ordered) or 0(random)] [output file]" <<endl;
       abort();
    }


//declaration part
    int n; //spin matrix size
    n = atof(argv[1]); 
    int MC;
    MC = atof(argv[2]); // number of Monte Carlo samples
    char *filename;
    int initial_state;
    initial_state = atof(argv[3]); //ordered or random
    filename = argv[4]; //filename
    double beta; // 1/T, where T is temperature
    double delta_E, delta_M, temp_exp;
    double invers_period = 1./RAND_MAX;
    double start_temp = 2.0;
    double fin_temp = 2.6;
    //double temp_step = 0.02;
    int **spin = new int*[n];
      for ( int i = 0; i < n; ++i ) {
          spin[i] = new int[n];
    }
//initialization of main variables
    srand( time(NULL) );
    
    
    int n_t;
    int m = 8;
    double temp_step;
    n_t = m * numprocs;
    temp_step = (fin_temp - start_temp)/(n_t - 1);

    double T[n_t];
    for ( int i = 0; i < n_t; i++ ) {
        T[i]=start_temp + i*temp_step;
    }
    
    //T = 2.4;
    //beta = 1./T;
//precalculate exponents
//    vector<double> expEnergies;
//    expEnergies.resize(16,0);
//    for(int i=-8; i<=8; i+=4) {
//	expEnergies[i+8] = exp(-beta*i);
//    } 
//Initialize spin matrix    
    state_init (n, spin, initial_state);
//     cout <<"Initial matrix in main programm:"<<endl;
//    for ( int i=0; i<n; i++ ) {
//        for ( int j=0; j<n; j++ ) {
//            cout << spin[i][j] <<"  ";
//        }
//        cout <<endl;
//    }

// Calculate brute force energy of the system after initialization
    double E2 = 0.0; double E = 0.0; double M = 0.0; double M2 = 0.0;
    energy_of_state_brute_force(n, spin, E, E2, M, M2);
    double E_sum = E;
    double M_sum = M;
    double corel_E_sum;
//rotate one spin
   int start_index;
   int stop_index;
   start_index = m*my_rank;
   stop_index = start_index + m;
   
   for ( int i = start_index; i < stop_index; i++   ) { //start temperature loop
       beta = 1./T[i];
       vector<double> expEnergies;
       expEnergies.resize(16,0);
       for ( int i=-8; i<=8; i+=4 ) {
           expEnergies[i+8] = exp(-beta*i);
        }
       int MC_1 = 0;
       for ( int s = 1; s <= MC; s++ ) { //start of Monte Carlo simulations
           MC_1++;
           int rand_spin_i =  (rand())*invers_period*n;
           int rand_spin_j =  (rand())*invers_period*n;
           spin[rand_spin_i][rand_spin_j] *= -1;
           delta_energy_by_spin_flipping(n, rand_spin_i, rand_spin_j, spin, delta_E, delta_M);
           if ( delta_E <= 0 ) {

               //accept configuration
              update_expect(E, delta_E, E2, E_sum, M, delta_M, M2, M_sum);
              //write_to_file(filename, E_sum, M_sum, s);
           } else {
        
     	       temp_exp = expEnergies[delta_E+8];
               double temp_rand = (rand())*invers_period;
               if ( temp_exp >= temp_rand ) {
                  update_expect(E, delta_E, E2, E_sum, M, delta_M, M2, M_sum);
                // write_to_file(filename, E_sum, M_sum, s);
               } else {
                 //##### flip back spin
                 spin[rand_spin_i][rand_spin_j] *= -1;
                 delta_E = 0.; delta_M = 0.;
                 update_expect(E, delta_E, E2, E_sum, M, delta_M, M2, M_sum);
                 //write_to_file(filename, E_sum, M_sum, s);
                 //do not update expectations  
               }
            }

   //pseudocorrelation
   /*if ( s == MC*0.07 ) {
      corel_E_sum = E_sum/(MC*0.07);
      //double corel_M_sum = M_sum/(MC*0.07);
      //double corel_E2 = E2/(MC*0.07);
      //double corel_M2 = M2/(MC*0.07);
   }*/
   if ( s == MC*0.10 ) {
      double corel_E_sum_1 = E_sum;
      double corel_M_sum_1 = M_sum;
      double corel_E2_1 = E2;
      double corel_M2_1 = M2;
      //if (abs((corel_E_sum_1/(MC*0.13) - corel_E_sum)/corel_E_sum_1) < 0.05) {
        E_sum = 0; M_sum = 0; E2 = 0; M2 = 0;
         //MC_1=0;    
   //}
   MC_1=0;
   }
   //MC_1++;
   } //End of Monte Carlo simulations

   /*cout <<"MC  "<<MC<<endl;
   cout <<"MC_1  "<<MC_1<<endl;
   cout <<"E   "<<(double)E_sum/MC_1<<endl;
   cout <<"E2  "<<(double)E2/MC_1<<endl;
   E2 = (double) E2/MC_1;
   double E_sum_2 = (double) (E_sum/MC_1)*(E_sum/MC_1);
   double variance = E2 - E_sum_2;
   cout <<"energy VARIANCE  "<<variance<<endl;
   double C_v = (variance)/(T*T);
   cout <<"C_v "<< (variance)/(T*T)<<endl;

   cout <<"MC  "<<MC<<endl;
   cout <<"M   "<<(double)M_sum/MC_1<<endl;
   cout <<"M2  "<<(double)M2/MC_1<<endl;
   M2 = (double) M2/MC_1;
   double M_sum_2 = (double) (M_sum/MC_1)*(M_sum/MC_1);
   double variance_m = M2 - M_sum_2;
   cout <<"magnetization VARIANCE  "<<variance_m<<endl;
   cout <<"Xi "<< (variance_m)/(T)<<endl;
   double Xi=(variance_m)/(T);
   //write_to_file(filename, E_sum, M_sum, C_v, Xi, T);*/
   }//end of temp loop
//free memory
for( int i = 0; i < n; ++i ) {
    delete [] spin[i];
}
delete [] spin;
MPI_Finalize ();

} 

////////////////////////////////#############################################////////////////////////////////////
////////////////////////////////############ FUNCTION SECTION ###############////////////////////////////////////
////////////////////////////////#############################################////////////////////////////////////

//Calculate energy on state brute force
void energy_of_state_brute_force (int n, int **spin, double &E, double &E2, double &M, double &M2 )
{
     for ( int i = 0; i < n; i++ ) {
         int j_max = n-1;
         for ( int j = 0; j < n; j++){
             E -=  (double) spin[i][j]*spin[i][j_max];
             j_max=j;
         }
     }

     for ( int j = 0; j < n; j++ ) {
         int i_max = n-1;
         for ( int i = 0; i < n; i++){
             E -=  (double) spin[i][j]*spin[i_max][j];
             i_max=i;
         }
     }
     
     E2 = E*E;

     for ( int i = 0; i < n; i++ ) {
         for ( int j = 0; j < n; j++ ) {
         M += spin[i][j];
         }
     }
     
     M2 = M*M;
}
//end on energy of state

///////////////////////////////////##########################################################////////////////////////////////////////

// function calculates energy difference with lattice with periodic boundary conditions.
void delta_energy_by_spin_flipping(int n, int i, int j, int **spin, double &delta_E, double &delta_M)
{
     double E_vert, E_horiz;
     int rand_spin_i = i;
     int rand_spin_j = j;
     int im;
     if ( i == n-1 ) {
        im = 0;
        E_vert  = spin[i-1][j] + spin[im][j];
      } else if ( i == 0 ) {
        im = n-1;
        E_vert  = spin[im][j] + spin[i+1][j];
      } else {
        E_vert  = spin[i-1][j] + spin[i+1][j];
      }
     int jm;
     if ( j == n-1 ) {
        jm = 0;
        E_horiz = spin[i][jm] + spin[i][j-1];
      } else if ( j == 0 ) {
        jm = n-1;
        E_horiz = spin[i][j+1] + spin[i][jm];
      } else {
        E_horiz = spin[i][j+1] + spin[i][j-1];
      }
     delta_E =- 2.*spin[rand_spin_i][rand_spin_j]*(E_vert + E_horiz);
     delta_M = 2.*spin[i][j];
}
//end of delta_energy_by_spin_flipping

///////////////////////////////////##########################################################////////////////////////////////////////

//Update expectations variables
void update_expect( double &E, double &delta_E, double &E2, double &E_sum, double &M, double &delta_M, double &M2, double &M_sum ) {

     double E_newstate = E + delta_E;
     E_sum = E_sum + E_newstate;
     E = E_newstate;
     E2 = E2 + E_newstate*E_newstate;
     double M_newstate = M + delta_M;
     M_sum = abs(M_sum) + abs(M_newstate);
     M = M_newstate;
     M2 = M2 + M_newstate*M_newstate; 
}
//end of update_expect

///////////////////////////////////##########################################################////////////////////////////////////////

// write to file function; 3 arguments - filename, energy, magnetization, number of cycle
/*void write_to_file ( char *filename, double E, double M, int s ) {
       ofstream ofile;
       ofile.open(filename, ios::app);
       ofile << (double) E/s<<","<< (double) M/s<<","<<s<<endl;
       ofile.close();
}*/
void write_to_file ( char *filename, double E, double M, double C_v, double Xi, double T ) {
       ofstream ofile;
       ofile.open(filename, ios::app);
       ofile <<E<<","<<M<<","<<C_v<<","<<Xi<<","<<T<<endl;
       ofile.close();
}

//end of write_to_fil

void state_init ( int n, int **spin, int initial_state ) {
     
     double invers_period = 1./RAND_MAX;
     if ( initial_state == 1 ) {  //initialize matrix start at low T, boring state :)
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++){
               spin[i][j]=1;
            }
        }
     }
     if ( initial_state == 0 ) {
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++){
               spin[i][j]=1;
            }
        }
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++){
                int rand_spin_i =  (rand())*invers_period*n;
                int rand_spin_j =  (rand())*invers_period*n;
                spin[rand_spin_i][rand_spin_j] *= -1;
            }
        }
     }

//test output: initial matrix
//    cout <<"Initial matrix :"<<endl;
//    for ( int i=0; i<n; i++ ) {
//        for ( int j=0; j<n; j++ ) {
//            cout << spin[i][j] <<"  ";
//        }
//        cout <<endl;
//    }
}

